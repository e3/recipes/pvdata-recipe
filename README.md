# pvData conda recipe

Home: https://github.com/epics-base/pvDataCPP

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS V4 C++ module
